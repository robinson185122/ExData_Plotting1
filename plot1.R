# Set Working Directory and see if the Directory "data" exists and if not then creates it

setwd("/Users/David/Documents/Coursera")
if (!file.exists("data")) {
        dir.create("data")
}

## Set URL Location and download the dataset

fileURL <- "https://d396qusza40orc.cloudfront.net/exdata%2Fdata%2Fhousehold_power_consumption.zip"
download.file(fileURL, destfile = "./data/household_power_consumption.zip", method = "curl")
list.files("./data")
datedownloaded <- date()
datedownloaded

## Unzip the file in the current working directory and will overwrite if exists
temp <- unzip(zipfile = "~/Documents/Coursera/data/household_power_consumption.zip",exdir = "~/Documents//Coursera/data/",overwrite = TRUE)

## Load the base data and then subset to include required date range
base_data <- read.table(file = temp,header = TRUE,sep = ";",na.strings = "?")
base_data$Date <- as.Date(x = base_data$Date,format = "%d/%m/%Y")
data <- subset(x = base_data,subset = (Date >= "2007-02-01" & Date <= "2007-02-02"))
rm(base_data)

## Convert Date / Time 
date_time <- paste(as.Date(data$Date), data$Time)
data$Datetime <- as.POSIXct(date_time)

## Create the Plot and Export as PNG
png(filename = "plot1.png",height = 480,width = 480,units = "px")

hist(x = data$Global_active_power,xlab = "Global Active Power (kilowatts)",ylab = "Frequency",main = "Global Active Power",col = "red")

dev.off()